package com.example.elearningTina.web.dto;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.model.Test;
import com.example.elearningTina.service.AnswerService;
import com.example.elearningTina.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ConverterDTO {

    @Autowired
    AnswerService answerService;

    @Autowired
     QuestionService questionService;

    public  AnswerDTO toDto(Answer answer) {

        return new AnswerDTO.AnswerDTOBuilder()
                .withAnswer(answer.getAnswer())
                .withId(answer.getId())
                .withQuestionId(answer.getQuestion().getId())
                .isCorrect(answer.isCorrect())
                .build();
    }

    public  List<AnswerDTO> toListOfDto(List<Answer> answers) {

        List<AnswerDTO> dtos = new ArrayList<>();

        for (Answer a: answers) {
            dtos.add(toDto(a));
        }
        return dtos;
    }

    public  List<Answer> toListOfAnswerEntities(List<AnswerDTO> dtos) {

        List<Answer> answers = new ArrayList<>();

        for (AnswerDTO dto: dtos) {
            answers.add(toEntityAnwserWithoutId(dto));  // Mozda sa id-jem??????????
        }
        return answers;
    }

    public  List<Answer> toListOfAnswerEntitiesWithId(List<AnswerDTO> dtos) {

        List<Answer> answers = new ArrayList<>();

        for (AnswerDTO dto: dtos) {
            answers.add(toEntityAnwserWithoutId(dto));   //BILO JE WITH A STAVILA SAM WITHOUT
        }
        return answers;
    }

    public  Answer toEntityAnwser(AnswerDTO dto) {


//        answer.setAnswer(dto.getAnswer());
//        answer.setCorrect(dto.isCorrect());
//        answer.setId(dto.getId());

        Optional<Question> question = questionService.findOne(dto.getQuestionId());

        Answer answer = new Answer(dto.getId(), dto.getAnswer(), question.get(), dto.isCorrect());

        return answer;
    }

    public  Answer toEntityAnwserWithoutId(AnswerDTO dto) {


//        answer.setAnswer(dto.getAnswer());
//        answer.setCorrect(dto.isCorrect());
//        answer.setId(dto.getId());

        Optional<Question> question = questionService.findOne(dto.getQuestionId());

        Answer answer = new Answer(dto.getAnswer(), question.get(), dto.isCorrect());

        return answer;
    }

    public Question toEntityQuestionWithoutId(QuestionDTO dto) {

        return new Question(dto.getQuestionText(), dto.getPoints(), dto.getQuestionType());
    }

    public Question toEntityQuestion(QuestionDTO dto) {

        return new Question(dto.getId(), dto.getQuestionText(), dto.getPoints(), dto.getQuestionType());
    }

    public Question toEntityQuestionWithAnswersList(QuestionDTO dto) {

        return new Question(dto.getId(), dto.getQuestionText(), dto.getPoints(),
                dto.getQuestionType(), toListOfAnswerEntities(dto.getAnswers()));
    }

    public Question toEntityQuestionWithAnswersListForTest(QuestionDTO dto) {

        return new Question(dto.getId(), dto.getQuestionText(), dto.getPoints(),
                dto.getQuestionType(), toListOfAnswerEntitiesWithId(dto.getAnswers()));
    }

    public List<Question> toListOfQuestions(List<QuestionDTO> questionDTOS) {

        List<Question> questions = new ArrayList<>();

        for (QuestionDTO dto: questionDTOS) {
            questions.add(toEntityQuestion(dto));
        }
        return questions;
    }

    public List<Question> toListOfQuestionsForTest(List<QuestionDTO> questionDTOS) {

        List<Question> questions = new ArrayList<>();

        for (QuestionDTO dto: questionDTOS) {
            questions.add(toEntityQuestionWithAnswersListForTest(dto));
        }
        return questions;
    }

    public Test toEntityTest(TestDTO newTestDTO) {

        return new Test(newTestDTO.getTitle(), toListOfQuestions(newTestDTO.getQuestions()));
    }
}
