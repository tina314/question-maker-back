package com.example.elearningTina.web.dto;

import com.example.elearningTina.model.Question;
import com.example.elearningTina.model.QuestionType;

import java.util.ArrayList;
import java.util.List;

public class QuestionDTO {

    private Long id;

    private String questionText;

    private int points;

    private QuestionType questionType;

    private List<AnswerDTO> answers;

    private QuestionDTO(Long id, String questionText, int points, QuestionType questionType, List<AnswerDTO> answers) {
        this.id = id;
        this.questionText = questionText;
        this.points = points;
        this.questionType = questionType;
        this.answers = answers;
    }


    public Long getId() {
        return id;
    }


    public String getQuestionText() {
        return questionText;
    }


    public int getPoints() {
        return points;
    }



    public QuestionType getQuestionType() {
        return questionType;
    }


    public List<AnswerDTO> getAnswers() {
        return answers;
    }

    public static QuestionDTO from(Question q) {
        return new QuestionDTO(q.getId(), q.getQuestionText(), q.getPoints(), q.getQuestionType(), AnswerDTO.from(q.getAnswers()));
    }

    public static List<QuestionDTO> from(List<Question> questions) {

        List<QuestionDTO> dtos = new ArrayList<>();

        for (Question question: questions) {
            dtos.add(from(question));
        }
        return dtos;
    }



}
