package com.example.elearningTina.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)  // PROBAJ AUTO
    private Long id;

    @Column(nullable = false)
    private String questionText;

    @Column(name = "points")
    private int points;

//    @Column(nullable = false)
//    private String questionType;

    @Enumerated(EnumType.STRING)
    private QuestionType questionType;

    @OneToMany(mappedBy = "question",  orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Answer> answers;

//    @ManyToMany(mappedBy = "questions")
//    private List<Test> tests;

    public Question() {}

    public Question(String questionText, int points, QuestionType questionType) {
        this.questionText = questionText;
        this.points = points;
        this.questionType = questionType;
        this.answers = new ArrayList<>(); //Da li je ovo dobra praksa?
    }

    public Question(Long id, String questionText, int points, QuestionType questionType) {
        this.id = id;
        this.questionText = questionText;
        this.points = points;
        this.questionType = questionType;
        this.answers = new ArrayList<>();
    }

    public Question(Long id, String questionText, int points, QuestionType questionType, List<Answer> answers) {
        this.id = id;
        this.questionText = questionText;
        this.points = points;
        this.questionType = questionType;
        this.answers = answers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addAnswer(Answer answer){
        this.answers.add(answer);

        if(answer.getQuestion()!=this){
            answer.setQuestion(this);
        }
    }

    public void removeAnswer(Answer answer){
        answer.setQuestion(null);
        answers.remove(answer);
    }

    //    public List<Test> getTests() {
//        return tests;
//    }
//
//    public void setTests(List<Test> tests) {
//        this.tests = tests;
//    }
}
