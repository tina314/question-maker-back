package com.example.elearningTina;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElearningTinaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElearningTinaApplication.class, args);
	}

}
