package com.example.elearningTina.repository;

import com.example.elearningTina.model.TestSolution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestSolutionRepository extends JpaRepository<TestSolution, Long> {

}
