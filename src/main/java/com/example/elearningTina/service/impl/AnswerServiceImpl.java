package com.example.elearningTina.service.impl;

import com.example.elearningTina.model.Answer;
import com.example.elearningTina.model.Question;
import com.example.elearningTina.repository.AnswerRepository;
import com.example.elearningTina.repository.QuestionRepository;
import com.example.elearningTina.service.AnswerService;
import com.example.elearningTina.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public Answer findOne(Long id) {
        return answerRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("message"));
    }

    @Override
    public List<Answer> findAll() {
        return answerRepository.findAll();
    }

    @Override
    public Answer save(Answer answer) {
        return answerRepository.save(answer);
    }

    @Override
    public List<Answer> save(List<Answer> answers) {
        return answerRepository.saveAll(answers);
    }

    @Override
    public void delete(Long id) {
         answerRepository.deleteById(id);
    }

    @Override
    public Answer save(long questionId, String answerT, boolean correct) {
        Question question = questionRepository.getOne(questionId);
        Answer answer = new Answer(answerT, question, correct);
        return answerRepository.save(answer);
    }
}
