package com.example.elearningTina.service.impl;

import com.example.elearningTina.model.Question;
import com.example.elearningTina.repository.AnswerRepository;
import com.example.elearningTina.repository.QuestionRepository;
import com.example.elearningTina.service.QuestionService;
import com.example.elearningTina.web.dto.ConverterDTO;
import com.example.elearningTina.web.dto.QuestionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    QuestionRepository questionRepository;

    @Autowired
    AnswerRepository answerRepository;

    @Autowired
    ConverterDTO converter;

    @Override
    public Optional<Question> findOne(Long id) {

        Optional<Question> question = questionRepository.findById(id);

        if (!question.isPresent()) {
            throw new EntityNotFoundException();
        }
        return question;
//        return questionRepository.findById(id)
//                .orElseThrow(() -> new EntityNotFoundException());
    }

    @Override
    public Question getById(Long id) {

        return questionRepository.getOne(id);
    }

    @Override
    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    @Override
    public Page<Question> findAll(Pageable pageable) {

        Page<Question> page = questionRepository.findAll(pageable);
        return questionRepository.findAll(pageable);
    }

//    @Override
//    public Page<ProductDTO> findAll(Pageable pageable) {
//        Page<Product> page = productRepository.findByEntityStatus(EntityStatus.REGULAR, pageable);
//        return new PageImpl<>(page.getContent()
//                .stream()
//                .map(product -> convertToDTO(product))
//                .collect(Collectors.toList()), pageable, page.getTotalElements());
//    }

    @Override
    public Question save(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public Question delete(Long id) {
        Question question = questionRepository.getOne(id);
        questionRepository.deleteById(id);
        return question;
    }
//   TINA
//    @Override
//    public Question edit(QuestionDTO questionDto) {
//        List<AnswerDTO> answerDTOS = questionDto.getAnswers();
//
//      //  Question persisted = questionRepository.save(converter.toEntityQuestion(questionDto));
//        Question question = findOne(questionDto.getId()).orElseThrow(() -> new IllegalArgumentException("There is no question"));
//        List<Answer> answers = converter.toListOfAnswerEntitiesWithId(answerDTOS);
////        answers.forEach(answer -> {
////            answer.setQuestion(question);
////            question.addAnswer(answer);
////        });
//        question.setQuestionText(questionDto.getQuestionText());
//        question.setPoints(questionDto.getPoints());
//        question.setQuestionType(questionDto.getQuestionType());
//        // answers = answerService.save(answers);
//
//        // question.setAnswers(answers);
//        Question persisted = questionRepository.save(question);
//        return persisted;
//    }

    //PEDJA
    @Override
    public Question edit(QuestionDTO questionDto) {
       // List<AnswerDTO> answerDTOS = questionDto.getAnswers();

        //  Question persisted = questionRepository.save(converter.toEntityQuestion(questionDto));
        findOne(questionDto.getId()).orElseThrow(() -> new IllegalArgumentException("There is no question"));

        Question question = converter.toEntityQuestionWithAnswersList(questionDto);
        // answers = answerService.save(answers);

        // question.setAnswers(answers);
        Question persisted = questionRepository.save(question);
        return persisted;
    }
}
