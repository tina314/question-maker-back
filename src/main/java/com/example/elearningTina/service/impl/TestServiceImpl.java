package com.example.elearningTina.service.impl;

import com.example.elearningTina.model.Test;
import com.example.elearningTina.repository.TestRepository;
import com.example.elearningTina.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class TestServiceImpl implements TestService {

    @Autowired
    private TestRepository testRepository;

    @Override
    public Test findOne(Long id) {
        return testRepository.getOne(id);
    }

    @Override
    public List<Test> findAll() {
        return testRepository.findAll();
    }

    @Override
    public Test save(Test test) {
        return testRepository.save(test);
    }

    @Override
    public List<Test> save(List<Test> tests) {
        return testRepository.saveAll(tests);
    }

    @Override
    public void delete(Long id) {
        testRepository.deleteById(id);
       // return testRepository.delete(findOne(id));
    }
}
