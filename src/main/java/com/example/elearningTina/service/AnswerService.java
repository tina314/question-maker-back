package com.example.elearningTina.service;

import com.example.elearningTina.model.Answer;

import java.util.List;

public interface AnswerService {

    Answer findOne(Long id);


    List<Answer> findAll();


    Answer save(Answer answer);


    List<Answer> save(List<Answer> answers);


    void delete(Long id);

    Answer save(long questionId, String answer, boolean correct);
}
